package gc_fraction_genome;
import java.util.*;
import java.io.*;
import java.lang.*;

public class Gc_fraction_genome 
{

    @SuppressWarnings("empty-statement")
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException 
    {        
    
        double gcfraction = 0; int valueGCnum = 0; int length = 0; int valueLength = 0;
        
        String inputFile = "5UTR.gc_content.txt";
        String getline;
        
        Scanner readfile = new Scanner(new File(inputFile));
        
        ArrayList transcriptID = new ArrayList<>();
        ArrayList geneID = new ArrayList<>();
        ArrayList geneName = new ArrayList<>();
 
        ArrayList <Integer> gcBasePairNum = new ArrayList<>();
        ArrayList <Integer> totalLength = new ArrayList<>();
        ArrayList <Double> gcFractions = new ArrayList<>();
        
        while (readfile.hasNextLine())
        {
            getline = readfile.nextLine();
            
            String array[] = getline.split(" ");
            transcriptID.add(array[0]);
            geneID.add(array[1]);
            geneName.add(array[2]);
            
            valueGCnum = Integer.parseInt(array[3]);
            gcBasePairNum.add(valueGCnum);
            valueLength = Integer.parseInt(array[4]);
            totalLength.add(length);  
            
            gcfraction = (double) valueGCnum / (double) valueLength;
            gcFractions.add(gcfraction);
            
        }
 
        // display the output      
        //PrintWriter writer = new PrintWriter("prot_cod.TPM_pvalues_2.csv");
        
        //writer.println("Gene ID,Gene Short Name,p values (tl_bs & tl_ngf)");

        System.out.println("Gene ID,Gene Short Name,p values (tl_bs & tl_ngf)");
        
        for (int i = 0; i < geneID.size(); i++) 
        {
            /*writer.print(geneID.get(i) + ",");
            writer.print(geneName.get(i) + ",");     
            writer.print(p_bs.get(i));
            writer.println();
            */
            System.out.print(">" + transcriptID.get(i) + "_");
            System.out.print(geneID.get(i) + "_");  
            System.out.print(geneName.get(i) + ",");     
            System.out.print(gcFractions.get(i));
            System.out.println();
        }              

    }
    

}
