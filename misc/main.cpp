//triplets
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <algorithm>
#include <iomanip>
using namespace std;
struct codon_position{
//t
int TTT =0; int TTC = 0; int TTA = 0;
int TTG =0 ;int TCT = 0; int TCC =0;
int TCA =0; int TCG =0; int TAT = 0;
   int TAC = 0; int TAA = 0; int TAG = 0;
    int TGT = 0; int TGC = 0; int TGA = 0;
    int TGG = 0;
//C
 int CTT = 0; int CTC = 0; int CTA = 0; int CTG = 0;int CCT = 0;int CCC = 0;int CCA = 0;int CCG = 0;int CAT = 0; int CAA = 0;int CAC = 0;int CAG = 0;int CGT = 0;int CGA = 0;int CGG = 0;int CGC = 0;
//A
int ATT = 0; int ATC = 0;int ATA = 0;int ATG = 0;int ACT = 0;int ACC = 0;int ACA = 0;int ACG = 0;int AAT = 0;int AAA = 0;int AAC = 0;int AAG = 0;int AGT = 0;int AGA = 0;int AGG = 0;int AGC = 0;
//G
int GTT = 0;int GTC = 0;int GTA = 0;int GTG = 0;int GCT = 0;int GCC = 0;int GCA = 0;int GCG = 0;int GAT = 0;int GAA = 0;int GAC = 0;int GAG = 0;int GGT = 0;int GGA = 0;int GGG = 0;int GGC = 0;




};
int main()
{
    ifstream inputFile;
    ofstream outputFile;
    string inputFilename;
    string outputFilename;
    string filereader;
    vector <string> geneID;
    vector <string> sequences;
    vector <string> tripletID;




    
    //keep the order for all these as T,C,A,G
    //T
    tripletID.push_back("TTT"); tripletID.push_back("TTC"); tripletID.push_back("TTA");
    tripletID.push_back("TTG"); tripletID.push_back("TCT"); tripletID.push_back("TCC");
    tripletID.push_back("TCA"); tripletID.push_back("TCG"); tripletID.push_back("TAT");
    tripletID.push_back("TAC"); tripletID.push_back("TAA"); tripletID.push_back("TAG");
    tripletID.push_back("TGT"); tripletID.push_back("TGC"); tripletID.push_back("TGA");
    tripletID.push_back("TGG");
    //C
    tripletID.push_back("CTT"); tripletID.push_back("CTC"); tripletID.push_back("CTA");
    tripletID.push_back("CTG"); tripletID.push_back("CCT"); tripletID.push_back("CCC");
    tripletID.push_back("CCA"); tripletID.push_back("CCG"); tripletID.push_back("CAT");
    tripletID.push_back("CAC"); tripletID.push_back("CAA"); tripletID.push_back("CAG");
    tripletID.push_back("CGT"); tripletID.push_back("CGC"); tripletID.push_back("CGA");
    tripletID.push_back("CGG");
    //A
    tripletID.push_back("ATT"); tripletID.push_back("ATC"); tripletID.push_back("ATA");
    tripletID.push_back("ATG"); tripletID.push_back("ACT"); tripletID.push_back("ACC");
    tripletID.push_back("ACA"); tripletID.push_back("ACG"); tripletID.push_back("AAT");
    tripletID.push_back("AAC"); tripletID.push_back("AAA"); tripletID.push_back("AAG");
    tripletID.push_back("AGT"); tripletID.push_back("AGC"); tripletID.push_back("AGA");
    tripletID.push_back("AGG");
    //G
    tripletID.push_back("GTT"); tripletID.push_back("GTC"); tripletID.push_back("GTA");
    tripletID.push_back("GTG"); tripletID.push_back("GCT"); tripletID.push_back("GCC");
    tripletID.push_back("GCA"); tripletID.push_back("GCG"); tripletID.push_back("GAT");
    tripletID.push_back("GAC"); tripletID.push_back("GAA"); tripletID.push_back("GAG");
    tripletID.push_back("GGT"); tripletID.push_back("GGC"); tripletID.push_back("GGA");
    tripletID.push_back("GGG");
    
    //File input
    cout << "Input fastq file name: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    while(!inputFile)
    {
        cout << "Error: the file could not be located." << endl;
        cout << "Input fastq file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    while (getline(inputFile, filereader))
    {
        if (filereader.at(0) == '>')
        {
            geneID.push_back(filereader);
        }
        else
        {
            sequences.push_back(filereader);
        }
    }
    inputFile.close();
    if (geneID.size() != sequences.size())
    {
        cout << "The sizes of the vectors are not the same. Program will exit." << endl;
        exit(0);
    }
    else
    {
        cout << "File has been successfully read in. Now checking the triplets." << endl << endl;
    }
    
    //this is the part of the code that will read the sequences in from right to left and collect the corresponding triplets. Starting from the 20th base
    
    int count[18][64];
    
    for (int i = 0; i < geneID.size(); i++)
    {
        int j = 2;
        
        while (j < sequences.at(i).size())
        {
            char* codon = new char[3];
            codon[0] = sequences.at(i).at(j);
            codon[1] = sequences.at(i).at(j - 1);
            codon[2] = sequences.at(i).at(j - 2);
            string varCodon = codon;
            delete [] codon;
            
            int matrixCounter = j-2;

        
            //all codons starting with T
            
            if (varCodon == "TTT") //0
            {
                count[matrixCounter][0] += 1;
            }
            
            else if (varCodon == "TTC") //1
            {
                count[matrixCounter][1] += 1;
            }
            
            else if (varCodon == "TTA") //2
            {
                count[matrixCounter][2] += 1;
            }
            
            else if (varCodon == "TTG") //3
            {
                count[matrixCounter][3] += 1;
            }
            
            else if (varCodon == "TCT") //4
            {
                count[matrixCounter][4] += 1;
            }
            
            else if (varCodon == "TCC") //5
            {
                count[matrixCounter][5] += 1;
            }
            
            else if (varCodon == "TCA") //6
            {
                count[matrixCounter][6] += 1;
            }
            
            else if (varCodon == "TCG") //7
            {
                count[matrixCounter][7] += 1;
            }
            
            else if (varCodon == "TAT") //8
            {
                count[matrixCounter][8] += 1;
            }
            
            else if (varCodon == "TAC") //9
            {
                count[matrixCounter][9] += 1;
            }
            
            else if (varCodon == "TAA") //10
            {
                count[matrixCounter][10] += 1;
            }
            
            else if (varCodon == "TAG") //11
            {
                count[matrixCounter][11] += 1;
            }
            
            else if (varCodon == "TGT") //12
            {
                count[matrixCounter][12] += 1;
            }
            
            else if (varCodon == "TGC") //13
            {
                count[matrixCounter][13] += 1;
            }
         
            else if (varCodon == "TGA") //14
            {
                count[matrixCounter][14] += 1;
            }
            
            else if (varCodon == "TGG") //15
            {
                count[matrixCounter][15] += 1;
            }
            
            // all codons starting with C
            
            else if (varCodon == "CTT") //32
            {
                count[matrixCounter][16] += 1;
            }
            
            else if (varCodon == "CTC") //33
            {
                count[matrixCounter][17] += 1;
            }
            
            else if (varCodon == "CTA") //34
            {
                count[matrixCounter][18] += 1;
            }
            
            else if (varCodon == "CTG") //35
            {
                count[matrixCounter][19] += 1;
            }
            
            else if (varCodon == "CCT") //36
            {
                count[matrixCounter][20] += 1;
            }
            
            else if (varCodon == "CCC") //37
            {
                count[matrixCounter][21] += 1;
            }
            
            else if (varCodon == "CCA") //38
            {
                count[matrixCounter][22] += 1;
            }
            
            else if (varCodon == "CCG") //39
            {
                count[matrixCounter][23] += 1;
            }
            
            else if (varCodon == "CAT") //40
            {
                count[matrixCounter][24] += 1;
            }
            
            else if (varCodon == "CAC") //41
            {
                count[matrixCounter][25] += 1;
            }
            
            else if (varCodon == "CAA") //42
            {
                count[matrixCounter][26] += 1;
            }
            
            else if (varCodon == "CAG") //43
            {
                count[matrixCounter][27] += 1;
            }
            
            else if (varCodon == "CGT") //44
            {
                count[matrixCounter][28] += 1;
            }
            
            else if (varCodon == "CGC") //45
            {
                count[matrixCounter][29] += 1;
            }
            
            else if (varCodon == "CGA") //46
            {
                count[matrixCounter][30] += 1;
            }
            
            else if (varCodon == "CGG") //47
            {
                count[matrixCounter][31] += 1;
            }
            
            // all codons starting with A
            
            else if (varCodon == "ATT") //16
            {
                count[matrixCounter][32] += 1;
            }
            
            else if (varCodon == "ATC") //17
            {
                count[matrixCounter][33] += 1;
            }
            
            else if (varCodon == "ATA") //18
            {
                count[matrixCounter][34] += 1;
            }
            
            else if (varCodon == "ATG") //19
            {
                count[matrixCounter][35] += 1;
            }
            
            else if (varCodon == "ACT") //20
            {
                count[matrixCounter][36] += 1;
            }
            
            else if (varCodon == "ACC") //21
            {
                count[matrixCounter][37] += 1;
            }
            
            else if (varCodon == "ACA") //22
            {
                count[matrixCounter][38] += 1;
            }
            
            else if (varCodon == "ACG") //23
            {
                count[matrixCounter][39] += 1;
            }
            
            else if (varCodon == "AAT") //24
            {
                count[matrixCounter][40] += 1;
            }
            
            else if (varCodon == "AAC") //25
            {
                count[matrixCounter][41] += 1;
            }
            
            else if (varCodon == "AAA") //26
            {
                count[matrixCounter][42] += 1;
            }
            
            else if (varCodon == "AAG") //27
            {
                count[matrixCounter][43] += 1;
            }
            
            else if (varCodon == "AGT") //28
            {
                count[matrixCounter][44] += 1;
            }
            
            else if (varCodon == "AGC") //29
            {
                count[matrixCounter][45] += 1;
            }
            
            else if (varCodon == "AGA") //30
            {
                count[matrixCounter][46] += 1;
            }
            
            else if (varCodon == "AGG") //31
            {
                count[matrixCounter][47] += 1;
            }
            
            // all codons starting with G
            
            else if (varCodon == "GTT") //48
            {
                count[matrixCounter][48] += 1;
            }
            
            else if (varCodon == "GTC") //49
            {
                count[matrixCounter][49] += 1;
            }
            
            else if (varCodon == "GTA") //50
            {
                count[matrixCounter][50] += 1;
            }
            
            else if (varCodon == "GTG") //51
            {
                count[matrixCounter][51] += 1;
            }
            
            else if (varCodon == "GCT") //52
            {
                count[matrixCounter][52] += 1;
            }
            
            else if (varCodon == "GCC") //53
            {
                count[matrixCounter][53] += 1;
            }
            
            else if (varCodon == "GCA") //54
            {
                count[matrixCounter][54] += 1;
            }
            
            else if (varCodon == "GCG") //55
            {
                count[matrixCounter][55] += 1;
            }
            
            else if (varCodon == "GAT") //56
            {
                count[matrixCounter][56] += 1;
            }
            
            else if (varCodon == "GAC") //57
            {
                count[matrixCounter][57] += 1;
            }
            
            else if (varCodon == "GAA") //58
            {
                count[matrixCounter][58] += 1;
            }
            
            else if (varCodon == "GAG") //59
            {
                count[matrixCounter][59] += 1;
            }
            
            else if (varCodon == "GGT") //60
            {
                count[matrixCounter][60] += 1;
            }
            
            else if (varCodon == "GGC") //61
            {
                count[matrixCounter][61] += 1;
            }
            
            else if (varCodon == "GGA") //62
            {
                count[matrixCounter][62] += 1;
            }
            
            else if (varCodon == "GGG") //63
            {
                count[matrixCounter][63] += 1;
            }
            
            //cout << varCodon << " ";
            
            j++;
        }
        
        //cout << endl;
    }

    //test output
    for (int i = 0; i < 18; i++)
    {
        cout << "Position " << i + 3 << ":" << endl;
        for (int j = 0; j < 64; j++)
        {
            if ((count[i][j] > 0) && (count[i][j] < geneID.size()))
            {
                cout << tripletID.at(j) << "," << count[i][j] << endl;
            }
        }
        cout << endl;
    }
    
    //create the output file
    return 0;
}
