#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <algorithm>
using namespace std;

int main()
{
    ifstream inputFile;
    ofstream outputFile;
    string inputFilename;
    string outputFilename;
    string filereader;
    vector <string> geneID;
    vector <string> sequences;
    
    cout << "Input fastq file name: ";
    
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());

    while(!inputFile)
    {
        cout << "Error: the file could not be located." << endl;
        cout << "Enter the input file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    int counter = 0;

    while (getline(inputFile, filereader))
    {
        if (counter == 0)
        {
            geneID.push_back(filereader);
            counter += 1;
        }
        
        else if (counter == 1)
        {
            sequences.push_back(filereader);
            counter += 1;
        }
        
        else if ((filereader.at(0) == '>') && (counter % 2 == 0) && (counter >= 2))
        {
            if (filereader == geneID.at(geneID.size()-1))
            {
                counter += 2;
            }
            
            else
            {
                bool duplicate = false;
                
                for (int i = 0; i < geneID.size(); i++)
                {
                    if (filereader == geneID.at(i))
                    {
                        duplicate = true;
                        counter += 2;
                    }
                }
                
                if (duplicate == false)
                {
                    geneID.push_back(filereader);
                    counter += 1;
                }
            }
            
        }
        
        else if (counter % 2 == 1)
        {
            if (filereader.length() < 24)
            {
                geneID.pop_back();
                counter += 1;
            }
            
            else if (filereader.length() > 24)
            {
                int numberOfExtraChars = (int)filereader.length() - 24;

                int charCounter = 0;
                while (charCounter < numberOfExtraChars)
                {
                    filereader.erase(0,1);
                    charCounter++;
                }
                
                sequences.push_back(filereader);
                counter += 1;
        
            }
            
            else
            {
                sequences.push_back(filereader);
                counter += 1;
            }
        }
    }
    
    inputFile.close();
    
    if (geneID.size() != sequences.size())
    {
        cout << "The sizes of the vectors are not the same. Program will exit." << endl;
        exit(0);
    }
    
  /*
    for (int i = 0; i < geneID.size(); i++)
    {
        geneID.at(i).pop_back();
    }
  
    for (int i = 0; i < geneID.size(); i++)
    {
        if (sequences.at(i).length() < 24)
        {
            int numberOfSpaces = 24 - (int)sequences.at(i).length();
            
            if (numberOfSpaces > 0)
            {
                int counter = 0;
                while (counter < numberOfSpaces)
                {
                    sequences.at(i) += "_";
                    counter++;
                }
            }
        }
    }
     */
    
    //file output
    
    int fastaFinder = (int)inputFilename.find(".fasta");
    
    if (!(fastaFinder == string::npos))
    {
        inputFilename.erase(fastaFinder, inputFilename.length() - fastaFinder);
    }
    
    outputFile.open(inputFilename + "_new.fasta");

    for (long int i = 0; i < geneID.size(); i++)
    {
        outputFile << geneID.at(i) << endl;
        outputFile << sequences.at(i) << endl;
    }
    outputFile.close();
    
    cout << "Text file has been created." << endl;
    return 0;
}
