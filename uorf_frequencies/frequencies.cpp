#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <algorithm>
#include <iomanip>
using namespace std;

int main()
{
    ifstream inputFile;
    ofstream outputFile;
    string inputFilename;
    string outputFilename;
    string filereader;
    
    vector <string> geneID;
    vector <string> sequences;
    
    vector <double> frequencyA;
    vector <double> frequencyC;
    vector <double> frequencyG;
    vector <double> frequencyT;
    
    cout << "Input fastq file name: ";
    
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    while(!inputFile)
    {
        cout << "Error: the file could not be located." << endl;
        cout << "Enter the input file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    while (getline(inputFile, filereader))
    {
        if (filereader.at(0) == '>')
        {
            geneID.push_back(filereader);
        }
        
        else
        {
            sequences.push_back(filereader);
        }
    }
    inputFile.close();
    if (geneID.size() != sequences.size())
    {
        cout << "The sizes of the vectors are not the same. Program will exit." << endl;
        exit(0);
    }
    
    ////this is the part of the code that will make sure each string has a length of 24 by including spaces at the ends of the strings.
    
    for (int i = 0; i < geneID.size(); i++)
    {
        if (sequences.at(i).length() < 24)
        {
            int numberOfSpaces = 24 - (int)sequences.at(i).length();
            
            if (numberOfSpaces > 0)
            {
                int counter = 0;
                while (counter < numberOfSpaces)
                {
                    sequences.at(i) += " ";
                    counter++;
                }
            }
        }
    }

    ///this is the part of the code that will account for the frequencies
    
    for (int i = 0; i < 24; i++)
    {
        double Acounter = 0, Gcounter = 0, Ccounter = 0, Tcounter = 0;
        
        for (int j = 0; j < geneID.size(); j++)
        {
            if (sequences.at(j).at(i) == 'A')
            {
                Acounter += 1;
            }
            
            else if (sequences.at(j).at(i) == 'G')
            {
                Gcounter += 1;
            }
            
            else if (sequences.at(j).at(i) == 'C')
            {
                Ccounter += 1;
            }
            
            else if (sequences.at(j).at(i) == 'T')
            {
                Tcounter += 1;
            }
            
        }
        
        frequencyA.push_back( (Acounter) / (geneID.size()) );
        frequencyG.push_back( (Gcounter) / (geneID.size()) );
        frequencyC.push_back( (Ccounter) / (geneID.size()) );
        frequencyT.push_back( (Tcounter) / (geneID.size()) );
        
    }
    
    int fastaFinder = (int)inputFilename.find(".fasta");
    
    if (!(fastaFinder == string::npos))
    {
        inputFilename.erase(fastaFinder, inputFilename.length() - fastaFinder);
    }
    
    outputFile.open(inputFilename + "_frequencies.fasta");
    outputFile << "File name: " << inputFilename << endl;
    outputFile << "Number of sequences: " << sequences.size() << endl << endl;
    
    for (long int i = 0; i < 24; i++)
    {
        outputFile << fixed << setprecision(5);
        outputFile << "Frequency of A at position " << i << ": " << frequencyA.at(i) << endl;
        outputFile << "Frequency of G at position " << i << ": " << frequencyG.at(i) << endl;
        outputFile << "Frequency of C at position " << i << ": " << frequencyC.at(i) << endl;
        outputFile << "Frequency of T at position " << i << ": " << frequencyT.at(i) << endl << endl;
    }
    outputFile.close();
     

    cout << "Text file has been created." << endl << endl;
     
    
    for (long int i = 0; i < 24; i++)
    {
        cout << frequencyA.at(i) << " ";
        cout << frequencyG.at(i) << " ";
        cout << frequencyC.at(i) << " ";
        cout << frequencyT.at(i) << endl;
    }
    
    return 0;
}
