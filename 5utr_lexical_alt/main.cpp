
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>

using namespace std;

int main()
{
    ifstream inputFile; ofstream outputFile;
    string inputFilename, outputFilename, sequenceName;
    
    vector <string> relativePosition;
    vector <string> transcriptID;
    vector <string> geneName;
    vector <string> chromosome;
    vector <string> UTR;
    vector <string> plusorminusLine;
    vector <int> relativeStart;
    vector <int> relativeEnd;
    vector <string> absolutePosition;
    vector <string> fullSequence;
    vector <int> readsAtPos1; vector <int> readsAtPos2; vector <int> readsAtPos3;
    vector <int> readsAtPos4; vector <int> readsAtPos5; vector <int> readsAtPos6;
    vector <int> readsAtPos7; vector <int> readsAtPos8; vector <int> readsAtPos9;
    vector <int> readsAtPos10; vector <int> readsAtPos11; vector <int> readsAtPos12;
    vector <int> readsAtPos13; vector <int> readsAtPos14; vector <int> readsAtPos15;
    
    //prompt user to enter in the fastq file name
    cout << "Input the fastq file name: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    //create a while loop that will display an error message until the program enters a correct file name or a file that's in the same directory
    
    while(!inputFile)
    {
        cout << "Error -- file could not be located." << endl;
        cout << "Enter the input file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    //create a while loop that will go through the file until it reaches the end
    while (!(inputFile.eof()))
    {
        int underscoreFinder = (int)sequenceName.find('_');
        int eFinder = (int)sequenceName.find("E");
        int slashFinder = (int)sequenceName.find('/');
        int minusFinder = (int)sequenceName.find('-');
        int plusFinder = (int)sequenceName.find('+');
        
        if (!(underscoreFinder == string::npos))
        {
            relativePosition.push_back(sequenceName);
        }
            
        else if(!(eFinder == string::npos))
        {
            transcriptID.push_back(sequenceName);
        }
            
        else
        {
            fullSequence.push_back(sequenceName);
        }
        
    }
    
    //close the file
    
    inputFile.close();

    
    /**********************************************************/
    //write the new sequences to a brand new text file with the same name as the original file but with a _final.fastq
    outputFile.open(inputFilename + "_final.fastq");
    
    //display the new lines of sequences
    for (long int i = 0; i < relativePosition.size(); i++)
    {
        outputFile << relativePosition.at(i) << endl;
        outputFile << transcriptID.at(i) << endl;
        outputFile << chromosome.at(i) << endl;
        outputFile << UTR.at(i) << endl;
        outputFile << plusorminusLine.at(i) << endl;
        outputFile << relativeStart.at(i) << endl;
        outputFile << relativeEnd.at(i) << endl;
        outputFile << absolutePosition.at(i) << endl;
        outputFile << fullSequence.at(i) << endl;
        outputFile << readsAtPos1.at(i) << endl;
        outputFile << readsAtPos2.at(i) << endl;
        outputFile << readsAtPos3.at(i) << endl;
        outputFile << readsAtPos4.at(i) << endl;
        outputFile << readsAtPos5.at(i) << endl;
        outputFile << readsAtPos6.at(i) << endl;
        outputFile << readsAtPos7.at(i) << endl;
        outputFile << readsAtPos8.at(i) << endl;
        outputFile << readsAtPos9.at(i) << endl;
        outputFile << readsAtPos10.at(i) << endl;
        outputFile << readsAtPos11.at(i) << endl;
        outputFile << readsAtPos12.at(i) << endl;
        outputFile << readsAtPos13.at(i) << endl;
        outputFile << readsAtPos14.at(i) << endl;
        outputFile << readsAtPos15.at(i) << endl;
        
    }
    
    outputFile.close();
    cout << inputFilename << "_final.fastq is now in your directory." << endl;
    return 0;
}
