#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <algorithm>
#include <regex>

using namespace std;

/*
using std::regex;
using std::string;
using std::sregex_token_iterator;
 */
// Delimiters are spaces (\s) and/or commas

int main()
{
    ifstream inputFile; ofstream outputFile;
    string outputFilename, sequenceName, numberSequence;
    
    double value_B6negRPF1_TPM = 0, value_B6negRPF2_TPM = 0, value_old_SWnegRPF1_TPM = 0, value_old_SWnegRPF2_TPM = 0;
    double value_SWnegRPF1_TPM = 0, value_SWnegRPF2_TPM = 0, value_SWnegRPF3_TPM = 0;
    double value_SWngfRPF1_TPM = 0, value_SWngfRPF2_TPM = 0, value_SWngfRPF3_TPM = 0, value_SWngfRPF4_TPM = 0, value_SWngfRPF5_FPKM = 0;
    
    double pvalue_bs_1to2 = 0, pvalue_bs_1to3 = 0, pvalue_bs_1to4 = 0, pvalue_bs_2to3 = 0, pvalue_bs_2to4 = 0, pvalue_bs_3to4 = 0;
    double pvalue_ngf_1to2 = 0, pvalue_ngf_1to3 = 0, pvalue_ngf_1to4 = 0, pvalue_ngf_2to3 = 0, pvalue_ngf_2to4 = 0, pvalue_ngf_3to4 = 0;
    
    vector <string> geneID;
    vector <string> geneName;
    vector <string> temp;
    vector <string> sequences;
    
    vector <double> B6negRPF1_TPM;
    vector <double> B6negRPF2_TPM;
    vector <double> old_SWnegRPF1_TPM;
    vector <double> old_SWnegRPF2_TPM;
    vector <double> SWnegRPF1_TPM;
    vector <double> SWnegRPF2_TPM;
    vector <double> SWnegRPF3_TPM;
    
    vector <double> SWngfRPF1_TPM;
    vector <double> SWngfRPF2_TPM;
    vector <double> SWngfRPF3_TPM;
    vector <double> SWngfRPF4_TPM;
    vector <double> SWngfRPF5_FPKM;
    
    
    //create vectors for the p-values that will be generated
    vector <double> p_bs_1to2;
    vector <double> p_bs_1to3;
    vector <double> p_bs_1to4;
    vector <double> p_bs_2to3;
    vector <double> p_bs_2to4;
    vector <double> p_bs_3to4;
    
    vector <double> p_ngf_1to2;
    vector <double> p_ngf_1to3;
    vector <double> p_ngf_1to4;
    vector <double> p_ngf_2to3;
    vector <double> p_ngf_2to4;
    vector <double> p_ngf_3to4;
    
    inputFile.open("prot_sample.txt");
    
    if(!inputFile.is_open())
    {
        cout << "Failed to open file." << endl;
        return 0;
    }
    
    //create a while loop that will go through the file until it reaches the end
    while (getline(inputFile, sequenceName))
    {
        getline(inputFile,sequenceName,',');
        geneID.push_back(sequenceName);
     
        getline(inputFile, sequenceName,',');
        geneName.push_back(sequenceName);
        
        getline(inputFile, sequenceName,',');
        value_B6negRPF1_TPM = stod(sequenceName);
        B6negRPF1_TPM.push_back(value_B6negRPF1_TPM);
        
        getline(inputFile, sequenceName,',');
        value_B6negRPF2_TPM = stod(sequenceName);
        B6negRPF2_TPM.push_back(value_B6negRPF2_TPM);
        
        getline(inputFile, sequenceName,',');
        value_old_SWnegRPF1_TPM = stod(sequenceName);
        old_SWnegRPF1_TPM.push_back(value_old_SWnegRPF1_TPM);
        
        getline(inputFile, sequenceName,',');
        value_old_SWnegRPF2_TPM = stod(sequenceName);
        old_SWnegRPF2_TPM.push_back(value_old_SWnegRPF2_TPM);
        
        getline(inputFile, sequenceName,',');
        value_SWnegRPF1_TPM = stod(sequenceName);
        SWnegRPF1_TPM.push_back(value_SWnegRPF1_TPM);
        
        getline(inputFile, sequenceName,',');
        value_SWnegRPF2_TPM = stod(sequenceName);
        SWnegRPF2_TPM.push_back(value_SWnegRPF2_TPM);
        
        getline(inputFile, sequenceName,',');
        value_SWnegRPF3_TPM = stod(sequenceName);
        SWnegRPF3_TPM.push_back(value_SWnegRPF3_TPM);
        
        getline(inputFile, sequenceName, ',');
        value_SWngfRPF1_TPM = stod(sequenceName);
        SWngfRPF1_TPM.push_back(value_SWngfRPF1_TPM);
        
        getline(inputFile, sequenceName, ',');
        value_SWngfRPF2_TPM = stod(sequenceName);
        SWngfRPF2_TPM.push_back(value_SWngfRPF2_TPM);
        
        getline(inputFile, sequenceName, ',');
        value_SWngfRPF3_TPM = stod(sequenceName);
        SWngfRPF3_TPM.push_back(value_SWngfRPF3_TPM);
        
        getline(inputFile, sequenceName, ',');
        value_SWngfRPF4_TPM = stod(sequenceName);
        SWngfRPF4_TPM.push_back(value_SWngfRPF4_TPM);
        
        getline(inputFile, sequenceName, '\n');
        value_SWngfRPF5_FPKM = stod(sequenceName);
        SWngfRPF5_FPKM.push_back(value_SWngfRPF5_FPKM);
     
        
        //////////////////////////////////////////////////////
        //another potential method?
        
     /*
        istringstream ss(sequenceName);
        string token;
        while (std::getline(ss, token, ','))
        {
            temp.push_back(token);
            cout << temp.size() << endl;
        }
        
        geneID.push_back(temp.at(0));
        geneName.push_back(temp.at(1));
        
        value_B6negRPF1_TPM = stod(temp.at(2));
        B6negRPF1_TPM.push_back(value_B6negRPF1_TPM);
        
        value_B6negRPF2_TPM = stod(temp.at(3));
        B6negRPF2_TPM.push_back(value_B6negRPF2_TPM);
        
        value_old_SWnegRPF1_TPM = stod(temp.at(4));
        old_SWnegRPF1_TPM.push_back(value_old_SWnegRPF1_TPM);
    
        value_old_SWnegRPF2_TPM = stod(temp.at(5));
        old_SWnegRPF2_TPM.push_back(value_old_SWnegRPF2_TPM);
        
        value_SWnegRPF1_TPM = stod(temp.at(6));
        SWnegRPF1_TPM.push_back(value_SWnegRPF1_TPM);
        
        value_SWnegRPF2_TPM = stod(temp.at(7));
        SWnegRPF2_TPM.push_back(value_SWnegRPF2_TPM);
        
        value_SWnegRPF3_TPM = stod(temp.at(8));
        SWnegRPF3_TPM.push_back(value_SWnegRPF3_TPM);
        
        value_SWngfRPF1_TPM = stod(temp.at(9));
        SWngfRPF1_TPM.push_back(value_SWngfRPF1_TPM);
        
        value_SWngfRPF2_TPM = stod(temp.at(10));
        SWngfRPF2_TPM.push_back(value_SWngfRPF2_TPM);
        
        value_SWngfRPF3_TPM = stod(temp.at(11));
        SWngfRPF3_TPM.push_back(value_SWngfRPF3_TPM);
        
        value_SWngfRPF4_TPM = stod(temp.at(12));
        SWngfRPF4_TPM.push_back(value_SWngfRPF4_TPM);
        
        value_SWngfRPF5_FPKM = stod(temp.at(13));
        SWngfRPF5_FPKM.push_back(value_SWngfRPF5_FPKM);

        temp.clear();
     */
        
    }
    
    //close the file
    
    inputFile.close();
    
    //cout << sequences.at(1) << endl;
    
    //int mu = 1;
    
    //double t_value = (xbar - mu)/(s/sqrt(n));
    
    //display the new lines of sequences
   /*
    
    outputFile.open("ptest_final.csv");
    
    cout << "Gene ID,Gene Short Name,p_bs_1to2,p_bs_1to3,p_bs_1to4,p_bs_2to3,p_bs_2to4,p_bs_3to4,p_ngf_1to2,p_ngf_1to3,p_ngf_1to4,p_ngf_2to3,p_ngf_2to4,p_ngf_3to4" << endl;
    
    for (long int i = 0; i < geneID.size(); i++)
    {
        cout << geneID.at(i) << ',';
        cout << geneName.at(i) << ',';
        cout << p_bs_1to2.at(i) << ',';
        cout << p_bs_1to3.at(i) << ',';
        cout << p_bs_1to4.at(i) << ',';
        cout << p_bs_2to3.at(i) << ',';
        cout << p_bs_2to4.at(i) << ',';
        cout << p_bs_3to4.at(i) << ',';
    
        cout << p_ngf_1to2.at(i) << ',';
        cout << p_ngf_1to3.at(i) << ',';
        cout << p_ngf_1to4.at(i) << ',';
        cout << p_ngf_2to3.at(i) << ',';
        cout << p_ngf_2to4.at(i) << ',';
        cout << p_ngf_3to4.at(i) << endl;
        
    }
    */
    
    return 0;
}

