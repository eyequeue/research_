#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <stdio.h>

using namespace std;

int main()
{
    ifstream inputFile;
    ofstream outputFile;
    string inputFilename, inputFile2, outputFilename, sequenceName;
    vector <string> sequenceID; vector <string> foundSequence;
    vector <string> fullSequence;

    //prompt user to enter in the fastq file name

    cout << "Input the file name with the original list: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    //create a while loop that will display an error message until the program enters a correct file name or a file that's in the same directory
    while(!inputFile)
    {
        cout << "Error -- file could not be located." << endl;
        cout << "Enter the input file name with the original list of sequence IDs: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    //create a while loop that will go through the text file until it reaches the end and collects the order of the sequences
    
    while (!inputFile.eof())
    {
        //read through each line as a string
        getline(inputFile, sequenceName);
        sequenceID.push_back(sequenceName);
    }

    inputFile.close();

    cout << "Input the file name with the unordered list of sequences: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    //create a while loop that will display an error message until the program enters a correct file name or a file that's in the same directory
    
    while(!inputFile)
    {
        cout << "Error -- file could not be located." << endl;
        cout << "Enter the input file name with the unordered list of sequence IDs: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    while (getline(inputFile, sequenceName))
    {
        fullSequence.push_back(sequenceName);
        
    }
    
    inputFile.close();
    
    for (int i = 0; i < sequenceID.size(); i++)
    {
        for (int j = 0; j < fullSequence.size(); j++)
        {
            int idFinder = (int)fullSequence.at(j).find(sequenceID.at(i));
            
            if(!(idFinder == string::npos))
            {
                foundSequence.push_back(fullSequence[j]);
            }

        }
    }
    
    
    int fastqFinder = (int)inputFilename.find(".fastq");
    
    if (!(fastqFinder == string::npos))
    {
        inputFilename.erase(fastqFinder, inputFilename.length() - fastqFinder);
    }
    
    
    outputFile.open(inputFilename + "_new.fastq");
    
    //display the new lines of sequences
    for (long int i = 0; i < foundSequence.size(); i++)
    {
        outputFile << foundSequence.at(i) << endl;
    }
    
    outputFile.close();
    cout << inputFilename << "_new.fastq is now in your directory." << endl;
    return 0;
}


