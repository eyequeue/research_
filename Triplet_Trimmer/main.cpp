//triplets
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <algorithm>
#include <iomanip>
using namespace std;
int main()
{
    ifstream inputFile;
    ofstream outputFile;
    string inputFilename;
    string outputFilename;
    string filereader;
    vector <string> geneID;
    vector <string> sequences;

    //File input
    cout << "Input fastq file name: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    while(!inputFile)
    {
        cout << "Error: the file could not be located." << endl;
        cout << "Input fastq file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    while (getline(inputFile, filereader))
    {
        if (filereader.at(0) == '>')
        {
            geneID.push_back(filereader);
        }
        else
        {
            filereader.erase(20,4);
            sequences.push_back(filereader);
        }
    }
    inputFile.close();
    if (geneID.size() != sequences.size())
    {
        cout << "The sizes of the vectors are not the same. Program will exit." << endl;
        exit(0);
    }
    else
    {
        cout << "File has been successfully read in. Now checking the triplets." << endl << endl;
    }
    
    for (int i = 0; i < geneID.size(); i++)
    {
        cout << geneID.at(i) << endl;
        cout << sequences.at(i) << endl;
        
    }
    
    //create the output file
    return 0;
}

