#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>

using namespace std;

int main()
{
    ifstream inputFile; ofstream outputFile;
    string inputFilename, outputFilename, sequenceName;
    
    vector <string> geneID;
    vector <string> geneName;
    vector <string> geneName;
    vector <string> chromosome;
    vector <string> UTR;
    vector <string> plusorminusLine;
    vector <int> relativeStart;
    vector <int> relativeEnd;
    vector <int> absolutePosition;
    vector <string> fullSequence;
    vector <int> readsAtPos1; vector <int> readsAtPos2; vector <int> readsAtPos3;
    vector <int> readsAtPos4; vector <int> readsAtPos5; vector <int> readsAtPos6;
    vector <int> readsAtPos7; vector <int> readsAtPos8; vector <int> readsAtPos9;
    vector <int> readsAtPos10; vector <int> readsAtPos11; vector <int> readsAtPos12;
    vector <int> readsAtPos13; vector <int> readsAtPos14; vector <int> readsAtPos15;
    
    vector <int> count;
    
    int counter = 0;
    
    //prompt user to enter in the fastq file name
    cout << "Input the fastq file name: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    //create a while loop that will display an error message until the program enters a correct file name or a file that's in the same directory
    
    while(!inputFile)
    {
        cout << "Error -- file could not be located." << endl;
        cout << "Enter file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    //create a while loop that will go through the file until it reaches the end
    while (!(inputFile.eof()))
    {
        getline(inputFile, sequenceName);
        relativePosition.push_back(sequenceName);
        
        getline(inputFile, sequenceName);
        transcriptID.push_back(sequenceName);
        
        getline(inputFile, sequenceName);
        geneName.push_back(sequenceName);
        
        getline(inputFile, sequenceName);
        chromosome.push_back(sequenceName);
        
        
        getline(inputFile, sequenceName);
        UTR.push_back(sequenceName);
        
        getline(inputFile, sequenceName);
        plusorminusLine.push_back(sequenceName);
        
        
        getline(inputFile, sequenceName);
        int start = atoi(sequenceName.c_str());
        relativeStart.push_back(start);
        
        
        getline(inputFile, sequenceName);
        int end = atoi(sequenceName.c_str());
        relativeEnd.push_back(end);
        
        
        getline(inputFile, sequenceName);
        int absolute = atoi(sequenceName.c_str());
        absolutePosition.push_back(absolute);
        
        
        getline(inputFile, sequenceName);
        fullSequence.push_back(sequenceName);
        
        
        getline(inputFile, sequenceName);
        int read1 = atoi(sequenceName.c_str());
        readsAtPos1.push_back(read1);
        
        getline(inputFile, sequenceName);
        int read2 = atoi(sequenceName.c_str());
        readsAtPos2.push_back(read2);
        
        getline(inputFile, sequenceName);
        int read3 = atoi(sequenceName.c_str());
        readsAtPos3.push_back(read3);
        
        
        getline(inputFile, sequenceName);
        int read4 = atoi(sequenceName.c_str());
        readsAtPos4.push_back(read4);
        
        
        getline(inputFile, sequenceName);
        int read5 = atoi(sequenceName.c_str());
        readsAtPos5.push_back(read5);
        
        
        getline(inputFile, sequenceName);
        int read6 = atoi(sequenceName.c_str());
        readsAtPos6.push_back(read6);
        
        
        getline(inputFile, sequenceName);
        int read7 = atoi(sequenceName.c_str());
        readsAtPos7.push_back(read7);
        
        
        getline(inputFile, sequenceName);
        int read8 = atoi(sequenceName.c_str());
        readsAtPos8.push_back(read8);
        
        
        getline(inputFile, sequenceName);
        int read9 = atoi(sequenceName.c_str());
        readsAtPos9.push_back(read9);
        
        
        getline(inputFile, sequenceName);
        int read10 = atoi(sequenceName.c_str());
        readsAtPos10.push_back(read10);
        
        
        getline(inputFile, sequenceName);
        int read11 = atoi(sequenceName.c_str());
        readsAtPos11.push_back(read11);
        
        getline(inputFile, sequenceName);
        int read12 = atoi(sequenceName.c_str());
        readsAtPos12.push_back(read12);
        
        
        getline(inputFile, sequenceName);
        int read13 = atoi(sequenceName.c_str());
        readsAtPos13.push_back(read13);
        
        getline(inputFile, sequenceName);
        int read14 = atoi(sequenceName.c_str());
        readsAtPos14.push_back(read14);
        
        getline(inputFile, sequenceName);
        int read15 = atoi(sequenceName.c_str());
        readsAtPos15.push_back(read15);
        
        count.push_back(counter + 1);
        
        cout << "Counter: " << count.at(counter) << endl;
        
        counter++;
        
    }
    
    //close the file
    
    inputFile.close();
    
    cout << "Relative Position Test: " << relativePosition.at(0) << endl;
    
    cout << "Transcript ID Test: " << transcriptID.at(8) << endl;
    

    
    //display the new lines of sequences
    for (long int i = 0; i < relativePosition.size(); i++)
    {
        cout << relativePosition.at(i) << endl;
        cout << transcriptID.at(i) << endl;
        cout << chromosome.at(i) << endl;
        cout << UTR.at(i) << endl;
        cout << plusorminusLine.at(i) << endl;
        cout << relativeStart.at(i) << endl;
        cout << relativeEnd.at(i) << endl;
        cout << absolutePosition.at(i) << endl;
        cout << fullSequence.at(i) << endl;
        cout << readsAtPos1.at(i) << endl;
        cout << readsAtPos2.at(i) << endl;
        cout << readsAtPos3.at(i) << endl;
        cout << readsAtPos4.at(i) << endl;
        cout << readsAtPos5.at(i) << endl;
        cout << readsAtPos6.at(i) << endl;
        cout << readsAtPos7.at(i) << endl;
        cout << readsAtPos8.at(i) << endl;
        cout << readsAtPos9.at(i) << endl;
        cout << readsAtPos10.at(i) << endl;
        cout << readsAtPos11.at(i) << endl;
        cout << readsAtPos12.at(i) << endl;
        cout << readsAtPos13.at(i) << endl;
        cout << readsAtPos14.at(i) << endl;
        cout << readsAtPos15.at(i) << endl;
        
    }
    
    return 0;
}
