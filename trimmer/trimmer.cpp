//trimmer program
//max length -- 35 to 55
//create 2 vectors that will read in the counts of a poly G being read from left to right and poly A being read from right to left

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>

using namespace std;

int main()
{
    ifstream inputFile; ofstream outputFile;
    string inputFilename, outputFilename, sequenceName;

    vector <string> sequenceID; vector <string> newSequenceID;
    vector <string> fullSequence; vector <string> trimSequence;
    vector <string> plusLine; vector <string> newPlus;
    vector <string> qualities; vector <string> newQualities;
    vector <int> polyACount; vector <int> polyGCount;
    vector <int> polyAPositionStart;
    vector <int> polyGPositionStart;
    
    //prompt user to enter in the fastq file name
    cout << "Input the fastq file name: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    //create a while loop that will display an error message until the program enters a correct file name or a file that's in the same directory
    while(!inputFile)
    {
        cout << "Error -- file could not be located." << endl;
        cout << "Enter the input file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    //create a while loop that will go through the file until it reaches the end
    while (getline(inputFile, sequenceName))
    {
        int eFinder = (int)sequenceName.find("E"), eaFinder = (int)sequenceName.find("EA");
        int aeFinder = (int)sequenceName.find("AE"), numberFinder = (int)sequenceName.find('#');
        int colonFinder = (int)sequenceName.find(':'), slashFinder = (int)sequenceName.find('/');
        int lessthanFinder = (int)sequenceName.find('<');
        int atFinder =(int)sequenceName.find("@"); int plusFinder = (int)sequenceName.find('+');

        if (!(atFinder == string::npos) && !(colonFinder == string::npos))
        {
            sequenceID.push_back(sequenceName);
        }

        else if(!(plusFinder == string::npos))
        {
            plusLine.push_back("+");
        }
        
        else if(!(eFinder == string::npos) || !(eaFinder == string::npos) || !(aeFinder == string::npos) ||
            !(numberFinder == string::npos) || !(slashFinder == string::npos) || !(lessthanFinder == string::npos))
        {
            qualities.push_back(sequenceName);
        }
        
        else
        {
            fullSequence.push_back(sequenceName);
        }

    }
    
    inputFile.close();
    
    if ( (sequenceID.size() != fullSequence.size()))
    {
        cout << "Error: sequenceID and fullSequence are not same size." << endl;
        
        if ((sequenceID.size() > fullSequence.size()))
        {
            cout << "sequenceID is bigger than fullSequence." << endl;
        }
        
        else if ((sequenceID.size() < fullSequence.size()))
        {
            cout << "sequenceID is smaller than fullSequence." << endl;
        }
        
        exit(0);
    }

    
    else if ((sequenceID.size() != qualities.size()))
    {
        cout << "Error: sequenceID and qualities are not same size." << endl;
        exit(0);
    }
    
    else if ((sequenceID.size() != plusLine.size()))
    {
        cout << "Error: sequenceID and plusLine are not same size." << endl;
        exit(0);
    }
    
    /******************************************************************/
    /*now we want to go through each of the sequences and analyze them to remove poly G's and A's, or completely remove everything about the sequence if there are less than 12 base pairs. */
    
    for (long int i = 0; i < sequenceID.size(); i++)
    {
        int maxA = 0, maxG = 0, maxAend = 0, maxGstart = 0, gCounter = 0, aCounter = 0;
        
        //create a for loop that will go through the sequences from left to right, counting the poly G's
        
        for (int forwardCount = 0; forwardCount < 10; forwardCount++)
        {
            if (fullSequence.at(i).at(forwardCount) == 'G' || fullSequence.at(i).at(forwardCount) == 'N'|| fullSequence.at(i).at(forwardCount) == 'C')
            {
                gCounter = 1;
                
                int gTracker = forwardCount + 1;
                
                while ((fullSequence.at(i).at(gTracker) == 'N' || fullSequence.at(i).at(gTracker) == 'G') && (gTracker < 10))
                {
                    gCounter++;
                    gTracker++;
                }
                
                if (maxG < gCounter && gCounter > 3)
                {
                    maxG = gCounter;
                    maxGstart = forwardCount;
                }
                
                else
                {
                    gCounter = 0;
                }
            }
            
            else
            {
                gCounter = 0;
            }
            
        }
        
        polyGCount.push_back(maxG);
        
        if (maxG > 3)
        {
            polyGPositionStart.push_back(maxGstart);
        }
        
        else
        {
            polyGPositionStart.push_back(0);
        }
        
        //create a for loop that will go through the sequences from right to left, counting the poly A's
        
        for (int reverseCount = (int)fullSequence.at(i).length() - 1; reverseCount >= (int)fullSequence.at(i).length() - 16; reverseCount--)
        {
            if ((fullSequence.at(i).at(reverseCount) == 'A') || (fullSequence.at(i).at(reverseCount) == 'N'))
            {
                aCounter = 1;
                int aTracker = reverseCount - 1;
                
                while ((fullSequence.at(i).at(aTracker) == 'N' || fullSequence.at(i).at(aTracker) == 'A') && aTracker >fullSequence.at(i).length() - 16)
                {
                    aCounter++;
                    aTracker--;
                }
                
                if (maxA < aCounter && aCounter > 3)
                {
                    maxA = aCounter;
                    maxAend = aTracker + 1;
                }
                
                else
                {
                    aCounter = 0;
                }
            }
            
            else
            {
                aCounter = 0;
            }
        }
        
        polyACount.push_back(maxA);
        
        if (maxA > 3)
        {
            polyAPositionStart.push_back(maxAend);
        }
        
        else
        {
            polyAPositionStart.push_back((int) fullSequence.at(i).length() - 1);
        }
    }
    
    int fastqFinder = (int)inputFilename.find(".fastq");
    
    if (!(fastqFinder == string::npos))
    {
        inputFilename.erase(fastqFinder, inputFilename.length() - fastqFinder);
    }
    
    /***************************************************************************************************/

    for (long int i = 0; i < sequenceID.size(); i++)
    {
        if ((polyACount.at(i) > 3 && polyACount.at(i) <= fullSequence.at(i).length()) && fullSequence.at(i).length() >= 15)
        {
            fullSequence.at(i).erase(polyAPositionStart.at(i), polyACount.at(i));
            qualities.at(i).erase(polyAPositionStart.at(i), polyACount.at(i));
            
            if (fullSequence.at(i).length() < 15)
            {
                continue;
            }
        }
            
        else if((polyACount.at(i) <= 3 && fullSequence.at(i).length() >= 15) || (fullSequence.at(i).length() < 15))
        {
            continue;
        }
    }
    
    //
    
    for (long int i = 0; i < sequenceID.size(); i++)
    {
        if ((polyGCount.at(i) > 3 && polyGCount.at(i) < fullSequence.at(i).length()) && fullSequence.at(i).length() >= 15)
        {
            fullSequence.at(i).erase(polyGPositionStart.at(i), polyGCount.at(i));
            qualities.at(i).erase(polyGPositionStart.at(i), polyGCount.at(i));
            
            if (fullSequence.at(i).length() < 15)
            {
                continue;
            }
            
            else
            {
                newSequenceID.push_back(sequenceID.at(i));
                trimSequence.push_back(fullSequence.at(i));
                newPlus.push_back("+");
                newQualities.push_back(qualities.at(i));
            }
        }
        
        else if (polyGCount.at(i) <= 3 && fullSequence.at(i).length() >= 15)
        {
            newSequenceID.push_back(sequenceID.at(i));
            trimSequence.push_back(fullSequence.at(i));
            newPlus.push_back("+");
            newQualities.push_back(qualities.at(i));
        }
        
        else if(fullSequence.at(i).length() < 15)
        {
            continue;
        }
    }
    
    if ((newSequenceID.size() != trimSequence.size()) || (newSequenceID.size() != newPlus.size()) || (newSequenceID.size() != newQualities.size()))
    {
        cout << "Error: new sequence vectors are not of the same size." << endl;
        exit(0);
    }
    
    /**********************************************************/
    //write the new sequences to a brand new text file with the same name as the original file but with a _final.fastq
    outputFile.open(inputFilename + "_final.fastq");
    
    //display the new lines of sequences
    for (long int i = 0; i < newSequenceID.size(); i++)
    {
        outputFile << newSequenceID.at(i) << endl;
        outputFile << trimSequence.at(i) << endl;
        outputFile << newPlus.at(i) << endl;
        outputFile << newQualities.at(i) << endl;
    }
    
    outputFile.close();
    cout << inputFilename << "_final.fastq is now in your directory." << endl;
    return 0;
}
