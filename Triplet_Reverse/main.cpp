#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <algorithm>
#include <iomanip>
using namespace std;

int main()
{
    ifstream inputFile;
    ofstream outputFile;
    string inputFilename;
    string filereader;
    vector <string> tripletReverse;
    
    //File input
    cout << "Input file name: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    while(!inputFile)
    {
        cout << "Error: the file could not be located." << endl;
        cout << "Input file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    while (getline(inputFile, filereader))
    {
        string readReverse;
        for (int i = (int)filereader.length() - 1; i >= 0 ; i--)
        {
            readReverse += filereader[i];
        }
        
        tripletReverse.push_back(readReverse);
    }
    
    inputFile.close();
    outputFile.open("neg_reverse.fasta");
    
    for (int i = 0; i < tripletReverse.size(); i++)
    {
        outputFile << tripletReverse.at(i) << endl;
    }
    
    return 0;
}
