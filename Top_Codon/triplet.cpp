//triplets
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <algorithm>
#include <iomanip>
using namespace std;

struct codon_position
{
    string name;
    //t
    int TTT =0; int TTC = 0; int TTA = 0; int TTG =0 ;int TCT = 0; int TCC =0; int TCA =0; int TCG =0; int TAT = 0; int TAC = 0;
    int TAA = 0; int TAG = 0; int TGT = 0; int TGC = 0; int TGA = 0; int TGG = 0;
    
    //C
    int CTT = 0; int CTC = 0; int CTA = 0; int CTG = 0;int CCT = 0;int CCC = 0;int CCA = 0;int CCG = 0;int CAT = 0; int CAA = 0;int CAC = 0;int CAG = 0;int CGT = 0;int CGA = 0;int CGG = 0;int CGC = 0;
    //A
    int ATT = 0; int ATC = 0;int ATA = 0;int ATG = 0;int ACT = 0;int ACC = 0;int ACA = 0;int ACG = 0;int AAT = 0;int AAA = 0;int AAC = 0;int AAG = 0;int AGT = 0;int AGA = 0;int AGG = 0;int AGC = 0;
    //G
    int GTT = 0;int GTC = 0;int GTA = 0;int GTG = 0;int GCT = 0;int GCC = 0;int GCA = 0;int GCG = 0;int GAT = 0;int GAA = 0;int GAC = 0;int GAG = 0;int GGT = 0;int GGA = 0;int GGG = 0;int GGC = 0;
};

void display_pos(codon_position &codon_pos)
{
    cout << "TTT," <<codon_pos.TTT << endl;
    cout << "TTC," <<codon_pos.TTC << endl;
    cout << "TTA," <<codon_pos.TTA << endl;
    cout << "TTG," <<codon_pos.TTG << endl;
    cout << "TCT," <<codon_pos.TCT << endl;
    cout << "TCC," <<codon_pos.TCC << endl;
    cout << "TCA," <<codon_pos.TCA << endl;
    cout << "TCG," <<codon_pos.TCG << endl;
    cout << "TAT," <<codon_pos.TAT << endl;
    cout << "TAC," <<codon_pos.TAC << endl;
    cout << "TAA," <<codon_pos.TAA << endl;
    cout << "TAG," <<codon_pos.TAG << endl;
    cout << "TGT," <<codon_pos.TGT << endl;
    cout << "TGC," <<codon_pos.TGC << endl;
    cout << "TGA," <<codon_pos.TGA << endl;
    cout << "TGG," <<codon_pos.TGG << endl;
    
    cout << "CTT," <<codon_pos.CTT << endl;
    cout << "CTC," <<codon_pos.CTC << endl;
    cout << "CTA," <<codon_pos.CTA << endl;
    cout << "CTG," <<codon_pos.CTG << endl;
    cout << "CCT," <<codon_pos.CCT << endl;
    cout << "CCC," <<codon_pos.CCC << endl;
    cout << "CCA," <<codon_pos.CCA << endl;
    cout << "CCG," <<codon_pos.CCG << endl;
    cout << "CAT," <<codon_pos.CAT << endl;
    cout << "CAC," <<codon_pos.CAC << endl;
    cout << "CAA," <<codon_pos.CAA << endl;
    cout << "CAG," <<codon_pos.CAG << endl;
    cout << "CGT," <<codon_pos.CGT << endl;
    cout << "CGC," <<codon_pos.CGC << endl;
    cout << "CGA," <<codon_pos.CGA << endl;
    cout << "CGG," <<codon_pos.CGG << endl;
    
    cout << "ATT," <<codon_pos.ATT << endl;
    cout << "ATC," <<codon_pos.ATC << endl;
    cout << "ATA," <<codon_pos.ATA << endl;
    cout << "ATG," <<codon_pos.ATG << endl;
    cout << "ACT," <<codon_pos.ACT << endl;
    cout << "ACC," <<codon_pos.ACC << endl;
    cout << "ACA," <<codon_pos.ACA << endl;
    cout << "ACG," <<codon_pos.ACG << endl;
    cout << "AAT," <<codon_pos.AAT << endl;
    cout << "AAC," <<codon_pos.AAC << endl;
    cout << "AAA," <<codon_pos.AAA << endl;
    cout << "AAG," <<codon_pos.AAG << endl;
    cout << "AGT," <<codon_pos.AGT << endl;
    cout << "AGC," <<codon_pos.AGC << endl;
    cout << "AGA," <<codon_pos.AGA << endl;
    cout << "AGG," <<codon_pos.AGG << endl;
    
    cout << "GTT," <<codon_pos.GTT << endl;
    cout << "GTC," <<codon_pos.GTC << endl;
    cout << "GTA," <<codon_pos.GTA << endl;
    cout << "GTG," <<codon_pos.GTG << endl;
    cout << "GCT," <<codon_pos.GCT << endl;
    cout << "GCC," <<codon_pos.GCC << endl;
    cout << "GCA," <<codon_pos.GCA << endl;
    cout << "GCG," <<codon_pos.GCG << endl;
    cout << "GAT," <<codon_pos.GAT << endl;
    cout << "GAC," <<codon_pos.GAC << endl;
    cout << "GAA," <<codon_pos.GAA << endl;
    cout << "GAG," <<codon_pos.GAG << endl;
    cout << "GGT," <<codon_pos.GGT << endl;
    cout << "GGC," <<codon_pos.GGC << endl;
    cout << "GGA," <<codon_pos.GGA << endl;
    cout << "GGG," <<codon_pos.GGG << endl;
}

void assign_pos(codon_position &codon_pos, string varCodon)
{
    //all codons starting with T
    
    if (varCodon == "TTT") //0
    {
        codon_pos.TTT += 1;
    }
    else if (varCodon == "TTC") //1
    {
        codon_pos.TTC += 1;
    }
    
    else if (varCodon == "TTA") //2
    {
        codon_pos.TTA += 1;
    }
    
    else if (varCodon == "TTG") //3
    {
        codon_pos.TTG += 1;
    }
    
    else if (varCodon == "TCT") //4
    {
        codon_pos.TCT += 1;
    }
    
    else if (varCodon == "TCC") //5
    {
        codon_pos.TCC += 1;
    }
    
    else if (varCodon == "TCA") //6
    {
        codon_pos.TCA += 1;
    }
    
    else if (varCodon == "TCG") //7
    {
        codon_pos.TCG += 1;
    }
    
    else if (varCodon == "TAT") //8
    {
        codon_pos.TAT += 1;
    }
    
    else if (varCodon == "TAC") //9
    {
        codon_pos.TAC += 1;
    }
    
    else if (varCodon == "TAA") //10
    {
        codon_pos.TAA += 1;
    }
    
    else if (varCodon == "TAG") //11
    {
        codon_pos.TAG += 1;
    }
    
    else if (varCodon == "TGT") //12
    {
        codon_pos.TGT += 1;
    }
    
    else if (varCodon == "TGC") //13
    {
        codon_pos.TGC += 1;
    }
    
    else if (varCodon == "TGA") //14
    {
        codon_pos.TGA += 1;
    }
    
    else if (varCodon == "TGG") //15
    {
        codon_pos.TGG += 1;
    }
    
    // all codons starting with C
    
    else if (varCodon == "CTT") //32
    {
        codon_pos.CTT += 1;
    }
    
    else if (varCodon == "CTC") //33
    {
        codon_pos.CTC += 1;
    }
    
    else if (varCodon == "CTA") //34
    {
        codon_pos.CTA += 1;
    }
    
    else if (varCodon == "CTG") //35
    {
        codon_pos.CTG += 1;
    }
    
    else if (varCodon == "CCT") //36
    {
        codon_pos.CCT += 1;
    }
    
    else if (varCodon == "CCC") //37
    {
        codon_pos.CCC += 1;
    }
    
    else if (varCodon == "CCA") //38
    {
        codon_pos.CCA += 1;
    }
    
    else if (varCodon == "CCG") //39
    {
        codon_pos.CCG += 1;
    }
    
    else if (varCodon == "CAT") //40
    {
        codon_pos.CAT += 1;
    }
    
    else if (varCodon == "CAC") //41
    {
        codon_pos.CAC += 1;
    }
    
    else if (varCodon == "CAA") //42
    {
        codon_pos.CAA += 1;
    }
    
    else if (varCodon == "CAG") //43
    {
        codon_pos.CAG += 1;
    }
    
    else if (varCodon == "CGT") //44
    {
        codon_pos.CGT += 1;
    }
    
    else if (varCodon == "CGC") //45
    {
        codon_pos.CGC += 1;
    }
    
    else if (varCodon == "CGA") //46
    {
        codon_pos.CGA += 1;
    }
    
    else if (varCodon == "CGG") //47
    {
        codon_pos.CGG += 1;
    }
    
    // all codons starting with A
    
    else if (varCodon == "ATT") //16
    {
        codon_pos.ATT += 1;
    }
    
    else if (varCodon == "ATC") //17
    {
        codon_pos.ATC += 1;
    }
    
    else if (varCodon == "ATA") //18
    {
        codon_pos.ATA += 1;
    }
    
    else if (varCodon == "ATG") //19
    {
        codon_pos.ATG += 1;
    }
    
    else if (varCodon == "ACT") //20
    {
        codon_pos.ACT += 1;
    }
    
    else if (varCodon == "ACC") //21
    {
        codon_pos.ACC += 1;
    }
    
    else if (varCodon == "ACA") //22
    {
        codon_pos.ACA += 1;
    }
    
    else if (varCodon == "ACG") //23
    {
        codon_pos.ACG += 1;
    }
    
    else if (varCodon == "AAT") //24
    {
        codon_pos.AAT += 1;
    }
    
    else if (varCodon == "AAC") //25
    {
        codon_pos.AAC += 1;
    }
    
    else if (varCodon == "AAA") //26
    {
        codon_pos.AAA += 1;
    }
    
    else if (varCodon == "AAG") //27
    {
        codon_pos.AAG += 1;
    }
    
    else if (varCodon == "AGT") //28
    {
        codon_pos.AGT += 1;
    }
    
    else if (varCodon == "AGC") //29
    {
        codon_pos.AGC += 1;
    }
    
    else if (varCodon == "AGA") //30
    {
        codon_pos.AGA += 1;
    }
    
    else if (varCodon == "AGG") //31
    {
        codon_pos.AGG += 1;
    }
    
    // all codons starting with G
    
    else if (varCodon == "GTT") //48
    {
        codon_pos.GTT += 1;
    }
    
    else if (varCodon == "GTC") //49
    {
        codon_pos.GTC += 1;
    }
    
    else if (varCodon == "GTA") //50
    {
        codon_pos.GTA += 1;
    }
    
    else if (varCodon == "GTG") //51
    {
        codon_pos.GTG += 1;
    }
    
    else if (varCodon == "GCT") //52
    {
        codon_pos.GCT += 1;
    }
    
    else if (varCodon == "GCC") //53
    {
        codon_pos.GCC += 1;
    }
    
    else if (varCodon == "GCA") //54
    {
        codon_pos.GCA += 1;
    }
    
    else if (varCodon == "GCG") //55
    {
        codon_pos.GCG += 1;
    }
    
    else if (varCodon == "GAT") //56
    {
        codon_pos.GAT += 1;
    }
    
    else if (varCodon == "GAC") //57
    {
        codon_pos.GAC += 1;
    }
    
    else if (varCodon == "GAA") //58
    {
        codon_pos.GAA += 1;
    }
    
    else if (varCodon == "GAG") //59
    {
        codon_pos.GAG += 1;
    }
    
    else if (varCodon == "GGT") //60
    {
        codon_pos.GGT += 1;
    }
    
    else if (varCodon == "GGC") //61
    {
        codon_pos.GGC += 1;
    }
    
    else if (varCodon == "GGA") //62
    {
        codon_pos.GGA += 1;
    }
    
    else if (varCodon == "GGG") //63
    {
        codon_pos.GGG += 1;
    }

}

//main function
int main()
{
    ifstream inputFile;
    string inputFilename;
    string filereader;
    vector <string> geneID;
    vector <string> sequences;
    vector <string> tripletID;
    
    //File input
    cout << "Input fastq file name: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    while(!inputFile)
    {
        cout << "Error: the file could not be located." << endl;
        cout << "Input fastq file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    while (getline(inputFile, filereader))
    {
        if (filereader.at(0) == '>')
        {
            geneID.push_back(filereader);
        }
        else
        {
            sequences.push_back(filereader);
        }
    }
    
    inputFile.close();
    if (geneID.size() != sequences.size())
    {
        cout << "The sizes of the vectors are not the same. Program will exit." << endl;
        exit(0);
    }
    
    else
    {
        cout << "File has been successfully read in. Now checking the triplets." << endl << endl;
    }
    
    //this is the part of the code that will read the sequences in from right to left and collect the corresponding triplets. Starting from the 20th base
    
    codon_position x[19];
    
    for (int i = 0; i < 19; i++)
    {
        codon_position y;
        y.name = "pos" + to_string(i + 3);
        x[i] = y;
    }
    
    for (int i = 0; i < geneID.size(); i++)
    {
        int j = 2;
        
        while (j < 20)
        {
            char* codon = new char[3];
            codon[0] = sequences.at(i).at(j - 2);
            codon[1] = sequences.at(i).at(j - 1);
            codon[2] = sequences.at(i).at(j);
            string varCodon = codon;
            delete [] codon;
            
            //
    
            for (int k = 0; k < 19; k++)
            {
                assign_pos(x[k], varCodon);
            }
            
            j++;
        }
    }
    
    for (int i = 0; i < 19; i++)
    {
        cout << x[i].name << ":" << endl;
        display_pos(x[i]);
        cout << endl;
    }
    
    //create the output file
    return 0;
}
