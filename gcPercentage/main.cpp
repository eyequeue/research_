//GC
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <sstream>
#include <algorithm>
#include <iomanip>
using namespace std;


//main function
int main()
{
    ifstream inputFile;
    string inputFilename;
    string filereader;
    vector <string> geneID;
    vector <string> sequences;
    vector <double> GCvector;
    
    //File input
    cout << "Input fastq file name: ";
    getline(cin,inputFilename);
    inputFile.open(inputFilename.c_str());
    
    while(!inputFile)
    {
        cout << "Error: the file could not be located." << endl;
        cout << "Input fastq file name: ";
        getline(cin, inputFilename);
        inputFile.open(inputFilename.c_str());
    }
    
    string sequence = "";
    
    while (getline(inputFile, filereader))
    {
        if (filereader.at(0) == '>')
        {
            geneID.push_back(filereader);
        }
        
        else
        {
            if (filereader == "Sequence unavailable")
            {
                geneID.pop_back();
                continue;
            }
            
            else
            {
                sequence += filereader;
                sequences.push_back(filereader);
            }
        }
    }
    
    //check to see if vector sizes are same
    inputFile.close();
    if (geneID.size() != sequences.size())
    {
        cout << "The sizes of the vectors are not the same. Program will exit." << endl;
        exit(0);
    }
    
    else
    {
        cout << "File has been successfully read in. Now calculating GC content." << endl << endl;
    }
    
    //calculate GC content
    for (int i = 0; i < geneID.size(); i++)
    {
        double gcCounter = 0;
        double gcPercentage = 0;
        
        for (int j = 0; j < sequences.at(i).length(); j++)
        {
            if (sequences.at(i).at(j) == 'G' || sequences.at(i).at(j) == 'C')
            {
                gcCounter++;
            }
        }
        
        gcPercentage = gcCounter / (double)sequences.at(i).length();
        
        GCvector.push_back(gcPercentage);
    }
    
    
    //output
    cout << "Gene" << "," << "GC fraction" << endl;
    for (int i = 0; i < geneID.size(); i++)
    {
        cout << geneID.at(i) << "," << GCvector.at(i) << endl;
    }
    
    //create the output file
    return 0;
}
